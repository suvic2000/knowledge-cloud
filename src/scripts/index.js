const open = (e) => e.hidden = !e.hidden;
const getTarget = (e) => document.getElementById(e.getAttribute('data-open'));
const addOpenListener = (e) => e.addEventListener('click', (event) => {
   event.preventDefault();
   open(getTarget(e))
});

document.querySelectorAll('[data-open]').forEach(addOpenListener);

document.getElementsById("trigger-button").addEventListener("click", function () {
   document.querySelector(".popup").getElementsByClassName.display = "flex";
});